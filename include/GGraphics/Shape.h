//
// Created by Harsh on 10-01-2022.
//
namespace GGraphics
{
#ifndef GAMEENGINE_OBJECTSHAPE_H
#define GAMEENGINE_OBJECTSHAPE_H

	enum class Shape
	{
		CIRCLE = 0,
		LINE = 1,
		BOX = 2
	};

#endif //GAMEENGINE_OBJECTSHAPE_H
}